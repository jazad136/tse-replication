// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#define _SILENCE_STDEXT_HASH_DEPRECATION_WARNINGS 1

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#define NOMINMAX
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <vector>
#include <algorithm>

namespace Gdiplus
{
  using std::min;
  using std::max;
};

// TODO: reference additional headers your program requires here
