#!/usr/bin/env 
# -*- coding: utf8 -*-

def handleError(function):
    """
    wrapper for handle exception
    """
    def handleProblems(*args, **kwargs):
        try:
            return function(*args, **kwargs)
        except Exception, e:
            #logger.error(e.message)
            print function, e

    return handleProblems

#@handleError
def list_slice(tuples, lambda_func):
    try:
        start, i = 0, 1
        pre = lambda_func(tuples[start])

        slices = []
        while i < len(tuples):
            cur = lambda_func(tuples[i])
                
            if pre != cur:
                slices.append((pre, start, i-1))       
                start = i
                pre = lambda_func(tuples[start])

            i+=1

        aslice = []

        slices.append((pre, start, i-1))

        return slices
    except Exception, e:
        print tuples

def drange(start, stop, step):
    while start < stop:
        yield start
        start += step

def is_special_key(key):
    if key.find('Ctrl') >= 0 or key.find('Alt') >= 0 or key.find('Windows') >=0:
        return True
    elif key.startswith('F') and key[1:len(key)].isdigit():
        return True

    return False

def get_process_type(process_name):
    if process_name == 'eclipse.exe' or process_name == 'devenv.exe' or process_name == 'javaw.exe':
        return 'IDE'
    elif process_name == 'firefox.exe' or process_name == 'chrome.exe' or process_name == 'iexplore.exe':
        return 'Browser'
    elif process_name == 'WINWORD.EXE' or process_name == 'EXCEL.EXE' or process_name == 'POWERPNT.EXE':
        return 'Office'
    else:
        return 'Other'

if __name__ == '__main__':
    a= [(1,1),(1,1),(2,2),(2,3),(1,1),(2,2),(2,3),(2,3)]
    b = [{'p':'a'}, {'p':'a'},{'p':'b'},{'p':'a'},{'p':'a'},{'p':'b'},{'p':'a'},{'p':'a'}]

    print list_slice(a, lambda k:k[0])
    print list_slice(b, lambda k:k['p'])
