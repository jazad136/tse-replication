#!/usr/bin/env
# -*- coding: utf8 -*-

import getpass
import os
import sys
from datetime import datetime
import util
import time_util
from sqlite_dbimpl import sqlite_query
import sqlite3
from application import *
import json
from collections import Counter
import xlsxwriter
import codecs
from django.utils.encoding import force_unicode

rt = 1


def get_process_category(process_name):
    if process_name in ('eclipse.exe', 'javaw.exe', 'devenv.exe'):
        return 'IDE'
    elif process_name in ('firefox.exe', 'chrome.exe', 'iexplore.exe'):
        return 'Browser'
    elif process_name in ('notepad.exe', 'notepad++.exe', 'sublime_text.exe'):
        return 'Text Editor'
    else:
        return 'Document'


class DataParser:
    def __init__(self, data_path):
        if data_path.endswith('.json'):
            self.jsonfile = data_path
            folder, filename = os.path.split(data_path)

            self.xlsfile = folder + '/' + filename[:-5] + '.xlsx'
        else:
            self.jsonfile = os.path.join(data_path, 'info.json')
            self.xlsfile = os.path.join(data_path, 'result.xlsx')

        with codecs.open(self.jsonfile, "r", "utf-8") as f:
            self.data = json.load(f)

        self.workbook = xlsxwriter.Workbook(self.xlsfile)
        self.workbook.encoding = 'utf-8'
        self.worksheet = self.workbook.add_worksheet('sheet1')
        self.worksheet2 = self.workbook.add_worksheet('sheet2')
        self.window_sheet = self.workbook.add_worksheet('Window Statistic')
        self.res_sheet = self.workbook.add_worksheet('overall')

        self.percentage_format = self.workbook.add_format(
            {'num_format': '0.00%'})
        self.double_format = self.workbook.add_format({'num_format': '0.00'})

    def close(self):
        self.workbook.close()

    def write_detail(self, day, activity_stat, line, overall=False):
        self.worksheet.write('A%d' % line, day)

        activity_stat['summary'] = {
            'Understanding': 0, 'Navigation': 0, 'Editing': 0, 'Other': 0, 'total': 0}
        activity_array = [(key, activity_stat[key]) for key in activity_stat]
        activity_array = sorted(activity_array, key=lambda k: k[0])
        for item in activity_array:
            p = item[0]
            if p == 'summary':
                continue

            other = item[1]['total'] - item[1]['Understanding'] - \
                item[1]['Navigation'] - item[1]['Editing']
            if other < 0:
                other = 0

            self.worksheet.write('B%d' % line, p)
            self.worksheet.write('C%d' % line, item[1]['Understanding'])
            self.worksheet.write('D%d' % line, item[1]['Navigation'])
            self.worksheet.write('E%d' % line, item[1]['Editing'])
            self.worksheet.write('F%d' % line, other)
            self.worksheet.write('G%d' % line, item[1]['total'])

            activity_stat['summary'][
                'Understanding'] += item[1]['Understanding']
            activity_stat['summary']['Navigation'] += item[1]['Navigation']
            activity_stat['summary']['Editing'] += item[1]['Editing']
            activity_stat['summary']['Other'] += other
            activity_stat['summary']['total'] += item[1]['total']
            if item[1]['total'] > 0:
                self.worksheet.write('H%d' % line, item[1][
                                     'Understanding'] / item[1]['total'], self.percentage_format)
                self.worksheet.write('I%d' % line, item[1][
                                     'Navigation'] / item[1]['total'], self.percentage_format)
                self.worksheet.write('J%d' % line, item[1][
                                     'Editing'] / item[1]['total'], self.percentage_format)
                self.worksheet.write(
                    'K%d' % line, other / item[1]['total'], self.percentage_format)
                self.worksheet.write('L%d' % line, '1', self.percentage_format)

            # if not overall:
            #    if p in self.overall_stat[day]:
            #        for c in activity_stat[p]:
            #            self.overall_stat[day][p][c] += activity_stat[p][c]
            #    else:
            #        self.overall_stat[day][p] = activity_stat[p]

            line += 1

        self.worksheet.write('B%d' % line, 'Summary')
        self.worksheet.write('C%d' % line, activity_stat[
                             'summary']['Understanding'])
        self.worksheet.write('D%d' % line, activity_stat[
                             'summary']['Navigation'])
        self.worksheet.write('E%d' % line, activity_stat['summary']['Editing'])
        self.worksheet.write('F%d' % line, activity_stat['summary']['Other'])
        self.worksheet.write('G%d' % line, activity_stat['summary']['total'])
        if activity_stat['summary']['total'] > 0:
            self.worksheet.write('H%d' % line, activity_stat['summary'][
                                 'Understanding'] / activity_stat['summary']['total'], self.percentage_format)
            self.worksheet.write('I%d' % line, activity_stat['summary'][
                                 'Navigation'] / activity_stat['summary']['total'], self.percentage_format)
            self.worksheet.write('J%d' % line, activity_stat['summary'][
                                 'Editing'] / activity_stat['summary']['total'], self.percentage_format)
            self.worksheet.write('K%d' % line, activity_stat['summary'][
                                 'Other'] / activity_stat['summary']['total'], self.percentage_format)
            self.worksheet.write('L%d' % line, '1', self.percentage_format)

        if overall and self.total_session_duration > 0:
            self.worksheet.write('K2', activity_stat['summary'][
                                 'Understanding'] / self.total_session_duration, self.percentage_format)
            self.worksheet.write('L2', activity_stat['summary']['Understanding'] / (
                self.total_session_duration + self.short_afk), self.percentage_format)

            self.res_sheet.write('L1', activity_stat['summary'][
                                 'Understanding'] / self.total_session_duration, self.percentage_format)
            self.res_sheet.write('M1', activity_stat['summary']['Understanding'] / (
                self.total_session_duration + self.short_afk), self.percentage_format)
            self.res_sheet.write('N1', activity_stat['summary']['Understanding'] / (
                activity_stat['summary']['total']), self.percentage_format)
            self.res_sheet.write('O1', activity_stat['summary'][
                                 'Navigation'] / activity_stat['summary']['total'], self.percentage_format)
            self.res_sheet.write('P1', activity_stat['summary'][
                                 'Editing'] / activity_stat['summary']['total'], self.percentage_format)
            self.res_sheet.write('Q1', activity_stat['summary'][
                                 'Other'] / activity_stat['summary']['total'], self.percentage_format)
            

        return line + 2

    def parse_sheet1(self):
        #data_path = '../data/WON/fangwang_log'
        self.session_number = self.data['Session Number']
        idle_times = self.data['Idle Times']
        day_num = 0

        self.worksheet.write('A1', '#Day')
        self.worksheet.write('B1', '#MouseEvent')
        self.worksheet.write('C1', '#ClickEvent')
        self.worksheet.write('D1', '#KeyEvent')
        self.worksheet.write('E1', '#Session')
        self.worksheet.write('F1', 'Activity Time')
        self.worksheet.write('G1', 'Short AFK Time (<1 hour)')
        self.worksheet.write('H1', 'Long AFK Time')
        self.worksheet.write('I1', 'Working Hour')
        self.worksheet.write('J1', 'Working Hour(Average)')
        self.worksheet.write('K1', 'Comprehesion 1')
        self.worksheet.write('L1', 'Comprehesion 2')

        #self.worksheet.write('A2', len(self.data['days']))
        self.worksheet.write('B2', self.data['Mouse Event Number'])
        self.worksheet.write('C2', self.data['Mouse Click Number'])
        self.worksheet.write('D2', self.data['Key Event Number'])
        self.worksheet.write('E2', self.session_number)

        self.worksheet.write('C4', 'Understanding')
        self.worksheet.write('D4', 'Navigation')
        self.worksheet.write('E4', 'Editing')
        self.worksheet.write('F4', 'Other')
        self.worksheet.write('G4', 'Total')
        self.worksheet.write('H4', 'Understanding(%%)')
        self.worksheet.write('I4', 'Navigation(%%)')
        self.worksheet.write('J4', 'Editing(%%)')
        self.worksheet.write('K4', 'Other(%%)')
        self.worksheet.write('L4', 'Total(%%)')

        line = 5

        pre_day = None
        activity_stat = {}
        self.overall_stat = {'overall': {}}
        self.total_session_duration = 0
        for i in range(self.session_number):
            session = self.data['session_' + str(i)]
            day = session['day']
            self.total_session_duration += session['duration']

            if day not in self.overall_stat:
                self.overall_stat[day] = {}

            if day != pre_day:
                if pre_day and self.overall_stat[pre_day]:
                    line = self.write_detail(
                        pre_day, self.overall_stat[pre_day], line)
                    day_num += 1

            for p in session['activity_stat']:
                category = get_process_category(p)

                if category in self.overall_stat[day]:
                    for c in session['activity_stat'][p]:
                        self.overall_stat[day][category][
                            c] += session['activity_stat'][p][c]

                    self.overall_stat[day][category][
                        'total'] += session['process_stat'][p]
                else:
                    self.overall_stat[day][category] = {}
                    for c in session['activity_stat'][p]:
                        self.overall_stat[day][category][
                            c] = session['activity_stat'][p][c]
                    self.overall_stat[day][category][
                        'total'] = session['process_stat'][p]

            pre_day = day

        if pre_day and self.overall_stat[day]:
            line = self.write_detail(day, self.overall_stat[day], line)
            day_num += 1

        line += 1

        for day in self.overall_stat:
            if day == 'overall':
                continue

            for p in self.overall_stat[day]:
                if p in self.overall_stat['overall']:
                    for c in self.overall_stat[day][p]:
                        self.overall_stat['overall'][p][
                            c] += self.overall_stat[day][p][c]
                else:
                    self.overall_stat['overall'][p] = self.overall_stat[day][p]

        self.short_afk = 0
        long_afk = 0
        for idle in idle_times:
            if idle[2] > 60 * 60:
                long_afk += idle[2]
            else:
                self.short_afk += idle[2]

        self.workhour = (self.total_session_duration + self.short_afk) / 3600

        line = self.write_detail('Total', self.overall_stat[
                                 'overall'], line, True)

        self.worksheet.write('A2', day_num)
        self.worksheet.write('F2', self.total_session_duration)
        self.worksheet.write('G2', self.short_afk)
        self.worksheet.write('H2', long_afk)
        self.worksheet.write('I2', self.workhour)

        self.res_sheet.write('A1', day_num)
        self.res_sheet.write('B1', self.workhour)

        if day_num > 0:
            self.worksheet.write('J2', self.workhour / day_num)
            self.res_sheet.write('C1', self.workhour / day_num)

        self.res_sheet.write('D1', self.data['Mouse Event Number'])
        self.res_sheet.write('E1', self.data['Mouse Click Number'])
        self.res_sheet.write('F1', self.data['Key Event Number'])
        self.res_sheet.write('G1', self.session_number)

        self.res_sheet.write('H1', self.total_session_duration)
        self.res_sheet.write('I1', self.overall_stat['overall']['summary']['total'])
        self.res_sheet.write('J1', self.short_afk)
        self.res_sheet.write('K1', long_afk)


    def write_process_switch(self, day, process_switch, line, overall=False):
        self.worksheet2.write('A%d' % line, day)

        max_len = 0
        start_chr = 'B'
        for slen in process_switch:
            switch_dict = process_switch[slen]
            switch_array = [(key, switch_dict[key]) for key in switch_dict]
            switch_array = sorted(switch_array, key=lambda k: k[
                                  1]['count'], reverse=True)

            if max_len < len(switch_array):
                max_len = len(switch_array)

            temp_line = line
            for idx, switch in enumerate(switch_array):
                if idx > 10:
                    break

                keystr = switch[0]
                count = switch[1]['count']
                understanding = switch[1]['Understanding']
                #navigation = switch[1]['Navigation']
                #editing = switch[1]['Editing']
                #total = switch[1]['Total']

                self.worksheet2.write('%c%d' % (start_chr, temp_line), keystr)
                self.worksheet2.write(
                    '%c%d' % (chr(ord(start_chr) + 1), temp_line), count)
                self.worksheet2.write('%c%d' % (
                    chr(ord(start_chr) + 2), temp_line), understanding, self.double_format)
                self.worksheet2.write('%c%d' % (chr(ord(start_chr) + 3), temp_line), understanding /
                                      self.overall_stat[day]['summary']['Understanding'], self.percentage_format)

                if overall:
                    if keystr == 'Browser->IDE':
                        self.res_sheet.write('R1', understanding /
                                      self.overall_stat[day]['summary']['Understanding'], self.percentage_format)
                    elif keystr == 'IDE->Browser':
                        self.res_sheet.write('S1', understanding /
                                      self.overall_stat[day]['summary']['Understanding'], self.percentage_format)
                    elif keystr == 'Browser->Text Editor':
                        self.res_sheet.write('T1', understanding /
                                      self.overall_stat[day]['summary']['Understanding'], self.percentage_format)
                    elif keystr == 'Text Editor->Browser':
                        self.res_sheet.write('U1', understanding /
                                      self.overall_stat[day]['summary']['Understanding'], self.percentage_format)
                    elif keystr == 'Browser->IDE->Browser':
                        self.res_sheet.write('V1', understanding /
                                      self.overall_stat[day]['summary']['Understanding'], self.percentage_format)
                    elif keystr == 'IDE->Browser->IDE':
                        self.res_sheet.write('W1', understanding /
                                      self.overall_stat[day]['summary']['Understanding'], self.percentage_format)
                    elif keystr == 'Browser->IDE->Browser->IDE':
                        self.res_sheet.write('X1', understanding /
                                      self.overall_stat[day]['summary']['Understanding'], self.percentage_format)
                    elif keystr == 'IDE->Browser->IDE->Browser':
                        self.res_sheet.write('Y1', understanding /
                                      self.overall_stat[day]['summary']['Understanding'], self.percentage_format)

                temp_line += 1

            start_chr = chr(ord(start_chr) + 5)

        max_len = max_len if max_len < 10 else 10
        line += max_len + 1

        return line + 1

    def add_switch(self, stat_dict, switch, switch_info):
        procs = switch.split('->')

        slen = len(procs)

        if slen not in stat_dict:
            stat_dict[slen] = {}

        if switch not in stat_dict[slen]:
            stat_dict[slen][switch] = {}
            stat_dict[slen][switch]['count'] = 0
            stat_dict[slen][switch]['Understanding'] = 0
            #stat_dict[slen][switch]['Navigation'] = 0
            #stat_dict[slen][switch]['Editing'] = 0
            #stat_dict[slen][switch]['Total'] = 0

        stat_dict[slen][switch]['count'] += switch_info['count']
        stat_dict[slen][switch][
            'Understanding'] += switch_info['Understanding']
        #stat_dict[slen][switch]['Navigation'] += switch_info['Navigation']
        #stat_dict[slen][switch]['Editing'] += switch_info['Editing']
        #stat_dict[slen][switch]['Total'] += switch_info['Total']

    def parse_sheet2(self):
        line = 1

        self.overall_process_switch = {}
        process_switch_per_day = {}
        pre_day = None
        for i in range(self.session_number):
            session = self.data['session_' + str(i)]
            day = session['day']

            if pre_day == day:
                for slen in session['process_switch']:
                    for switch in session['process_switch'][slen]:
                        self.add_switch(process_switch_per_day, switch, session[
                                        'process_switch'][slen][switch])
                        self.add_switch(self.overall_process_switch, switch, session[
                                        'process_switch'][slen][switch])
            else:
                if pre_day:
                    line = self.write_process_switch(
                        pre_day, process_switch_per_day, line)

                process_switch_per_day = {}
                for slen in session['process_switch']:
                    for switch in session['process_switch'][slen]:
                        self.add_switch(process_switch_per_day, switch, session[
                                        'process_switch'][slen][switch])
                        self.add_switch(self.overall_process_switch, switch, session[
                                        'process_switch'][slen][switch])

            pre_day = day

        if pre_day and process_switch_per_day:
            line = self.write_process_switch(day, process_switch_per_day, line)

        self.total_swich_count = 0
        line = self.write_process_switch(
            'overall', self.overall_process_switch, line, True)

    def parse_window_sheet(self):
        overall_window_stat = {}
        for i in range(self.session_number):
            session = self.data['session_' + str(i)]
            window_stat = session['window_stat']

            for key_window in window_stat:
                if key_window not in overall_window_stat:
                    overall_window_stat[key_window] = 0
                overall_window_stat[key_window] += window_stat[key_window]

        line = 1
        window_array = [(key, overall_window_stat[key])
                        for key in overall_window_stat]
        window_array = sorted(window_array, key=lambda k: k[1], reverse=True)
        for window_info in window_array:
            index = window_info[0].find('/')
            process_name = window_info[0][0:index]
            window_name = window_info[0][index + 1:]
            #$window_name = window_name.encode('utf-8', 'ignore')
            # print process_name, window_name

            try:
                if window_name.startswith('http'):
                    continue

                self.window_sheet.write('A%d' % line, process_name)
                self.window_sheet.write(
                    'B%d' % line, force_unicode(window_name, 'utf-8'))
                # print window_name.encode('utf-8', 'ignore')
                self.window_sheet.write('C%d' % line, window_info[1])
            except Exception, e:
                print e.message
                # print window_name

            line += 1

    def parse_spree_interval(self):
        self.session_number = self.data['Session Number']
        for i in range(self.session_number):
            session = self.data['session_' + str(i)]
            spree_interval = session['spree_interval']

            print 'session ', i
            print session['duration']
            print len(spree_interval)

            comprehension_count = 0
            for interval in spree_interval:
                if interval > 1:
                    comprehension_count +=1

            print comprehension_count

            print session['spree']

if __name__ == '__main__':
    batch = False
    if batch:
        #logs = ['../data/WON', '../data/Argus', '../data/HTA', '../data/UED', '../data/Honda']
        logs = ['D:/baolingfeng/research/Hengtian201612/WON/']

        for log in logs:
            for jsonfile in os.listdir(log):
                if jsonfile.endswith('.json') is False:
                    continue

                print os.path.join(log, jsonfile)

                parser = DataParser(os.path.join(log, jsonfile))
                parser.parse_sheet1()
                parser.parse_sheet2()
                parser.parse_window_sheet()
                parser.close()
    else:
        data_path = 'E:/baolingfeng/research/Hengtian201612/WON/151080-log.json'
        # data_path = '../data/user1/log'
        parser = DataParser(data_path)
        parser.parse_spree_interval()
        # parser.parse_sheet1()
        # parser.parse_sheet2()
        # parser.parse_window_sheet()
        # parser.close()
