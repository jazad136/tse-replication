#!/usr/bin/env
# -*- coding: utf8 -*-

import os
import util
import time_util
from sqlite_dbimpl import sqlite_query
import sqlite3
from application import *
import json


IDLE_THRESHOLD = 5 * 60  # 5 minutes in icpc paper
APPS = load_applications("applications.csv")
APP_DICT = {app.process: app for app in APPS}


def get_process_category(process_name):
    if process_name in ('eclipse.exe', 'javaw.exe', 'devenv.exe'):
        return 'IDE'
    elif process_name in ('firefox.exe', 'chrome.exe', 'iexplore.exe'):
        return 'Browser'
    elif process_name in ('notepad.exe', 'notepad++.exe','sublime_text.exe'):
        return 'Text Editor'
    else:
        return 'Document'


def get_application(process_name):
        return APP_DICT[process_name] if process_name in APP_DICT else None


def get_event_category(e):
    if is_control('title bar', e['action_type']) or is_control('scroll bar', e['action_type']) \
            or is_control('tool bar', e['action_type']) or is_control('menu bar', e['action_type']) \
            or is_control('tab', e['action_type']) or is_control('tab item', e['action_type']) \
            or is_control('tool bar', e['action_parent_type']) or is_control('thumb', e['action_type']):
            return 'Browsing'

    elif e['process_name'] == 'devenv.exe':
        if e['action_name'].startswith('Find') == 0 or e['action_parent_name'].startswith('Find') == 0:
            return 'Searching'
        elif e['action_name'].find('Explorer') >= 0 or e['action_parent_name'].find('Explorer') >= 0:
            return 'Browsing'
        elif e['action_name'] == 'Outlining Margin' or e['action_parent_name'] == 'Outlining Margin':
            return 'Browsing'
        elif is_control('tree', e['action_type']) or is_control('scroll bar', e['action_type']) \
            or is_control('list', e['action_type']) or is_control('list item', e['action_type']) \
            or is_control('tree item', e['action_type']):
            return 'Browsing'
    elif e['process_name'] == 'eclipse.exe' or e['process_name'] == 'javaw.exe': 
        #since the bug can't get the parent component view, e.g. Project Explorer, Console, 
        if e['action_name'] == 'Find/Replace' or e['action_name'].find('Search') >= 0:
            return 'Searching'
        #elif is_control('tree', e['action_type']) or is_control('scroll bar', e['action_type']) \
        #    or is_control('list', e['action_type']) or is_control('list item', e['action_type']) \
        #    or is_control('tree item', e['action_type']):
        elif not(is_control('pane', e['action_type']) or is_control('window', e['action_type']) or\
            is_control('edit', e['action_type']) or is_control('document', e['action_type']) or\
            is_control('text', e['action_type'])):
            return 'Browsing'
    else:
        if e['action_name'].startswith('Find') == 0 or e['action_parent_name'].startswith('Find') \
            or e['action_name'].find('Search') >= 0:
            return 'Searching'

    return 'Understanding'
   


class EventManager(object):
    def __init__(self, data_path, rt=1):
        self.data_path = data_path
        isdbfile = self.data_path.endswith('.db3')

        if isdbfile > 0:
            self.dbfile = data_path
            folder, filename = os.path.split(self.dbfile)
            self.jsonfile = folder + '/' + filename[:-4] + '.json'
        else:
            self.dbfile = data_path + '/log.db3'
            self.jsonfile = data_path + '/info.json'

        self.overall_stat = {}
        self.rt = rt
    
    def retrieve_events(self):
        conn = sqlite3.connect(self.dbfile, timeout=30)
        sql = 'select *, \'mouse\' as type from tbl_mouse_event'
        self.mouse_events = sqlite_query(conn, sql)
        self.overall_stat['Mouse Event Number'] = len(self.mouse_events)

        sql = 'select * from tbl_click_action'
        click_actions = sqlite_query(conn, sql)
        self.overall_stat['Mouse Click Number'] = len(click_actions)

        click_time_map = {c['timestamp']: idx for idx, c in enumerate(click_actions)}
        for i in range(len(self.mouse_events)):
            t = self.mouse_events[i]['timestamp']
            if t in click_time_map:
                self.mouse_events[i].update(click_actions[click_time_map[t]])

        sql = 'select *, \'key\' as type from tbl_key_event'
        self.key_events = sqlite_query(conn, sql)
        self.overall_stat['Key Event Number'] = len(self.key_events)

        sql = 'select * from tbl_key_action'
        key_actions = sqlite_query(conn, sql)
        key_time_map = {c['timestamp']: idx for idx, c in enumerate(key_actions)}
        for i, e in enumerate(self.key_events):
            t = e['timestamp']
            if t in key_time_map:
                e.update(key_actions[key_time_map[t]])

        strsql = 'select *, \'copy\' as type from tbl_copy_event'
        self.copy_events = sqlite_query(conn, strsql)

        self.combined_events = sorted(self.mouse_events + self.key_events, key=lambda k: k['timestamp'])
        self.combined_events = [e for e in self.combined_events if e['timestamp'].strip() != '']
        total_length = len(self.combined_events)

        strsql = 'select distinct substr(timestamp, 0, 11) as day from tbl_mouse_event'
        self.days = sqlite_query(conn, strsql)
        self.overall_stat['days'] = self.days

        session_start = 0
        self.sessions = []
        self.idle_times = []
        for i in range(0, total_length):
            if i == total_length - 1:
                self.combined_events[i]['duration'] = 0
                self.sessions.append((session_start, i))
            else:
                cur_t = self.combined_events[i]['timestamp']
                next_t = self.combined_events[i + 1]['timestamp']
                # print self.combined_events[i], self.combined_events[i + 1]
                interval = time_util.time_diff(cur_t, next_t)

                if interval > IDLE_THRESHOLD:
                    self.sessions.append((session_start, i))
                    session_start = i + 1
                    self.idle_times.append((cur_t, next_t, interval))

                    self.combined_events[i]['duration'] = 0
                else:
                    self.combined_events[i]['duration'] = interval

        self.overall_stat['Idle Times'] = self.idle_times

    def filter_no_need(self):
        self.filter_events = []
        self.process_stat = {'UNKNOWN': 0, 'explorer.exe': 0}
        self.session_event_list = []
        for session in self.sessions:
            si, ei = session
            session_events = []
            for i in range(si, ei + 1):
                e = self.combined_events[i]
                if e['process_name'] in ('UNKNOWN', 'explorer.exe'):
                    self.process_stat[e['process_name']] += e['duration']
                else:
                    if e['process_name'] in self.process_stat:
                        self.process_stat[e['process_name']] += e['duration']
                    else:
                        self.process_stat[e['process_name']] = e['duration']

                    session_events.append(e)

            if len(session_events) > 0:
                self.session_event_list.append(session_events)

    def run(self):
        self.retrieve_events()
        self.filter_no_need()

        session_number = len(self.session_event_list)
        self.overall_stat['Session Number'] = session_number

        self.overall_stat['overall'] = {}

        for idx, session_events in enumerate(self.session_event_list):
            session = ActivitySession(session_events)
            session_duration = session.session_duration()
            session_day = session.session_day()

            session.split_by_process()
            session.split_by_window()
            # print session.window_slices
            session.split_by_rt(self.rt)
            session.switch_stat()

            self.overall_stat['session_' + str(idx)] = {
                'day': session_day,
                'duration': session_duration,
                'activity_stat': session.activity_stat,
                'process_stat': session.process_stat,
                'process_switch': session.process_switch,
                'window_stat': session.window_stat,
                'spree_interval': session.spree_interval,
                'spree' : session.spree_categories
            }

        with open(self.jsonfile, 'w') as f:
            f.write(json.dumps(self.overall_stat).encode('utf-8'))


class ActivitySession:
    def __init__(self, session_events):
        self.spree_num = 0
        self.spree_interval = []
        self.spree_categories = {'Navigation' : 0, 'Understanding' : 0, 'Editing' : 0}
        self.session_events = session_events

    def session_duration(self):
        return time_util.time_diff(self.session_events[0]['timestamp'], self.session_events[-1]['timestamp'])

    def session_day(self):
        return self.session_events[0]['timestamp'][0:10]

    def split_by_process(self):
        self.process_slices = util.list_slice(self.session_events, lambda k: k['process_name'])

        self.sections = []
        for pslice in self.process_slices:
            self.sections.append({'process_name': pslice[0], 'start': pslice[1], 'end': pslice[2]})

    def split_by_window(self):
        self.window_slices = []
        last_prcess_window = {}

        self.process_stat = {}
        for pidx, psection in enumerate(self.sections):
            process_name = psection['process_name']
            start = psection['start']
            end = psection['end']
            psection['window_section'] = []
            app = get_application(process_name)

            process_duration = time_util.time_diff(self.session_events[start]['timestamp'], self.session_events[end]['timestamp'])
            if process_name in self.process_stat:
                self.process_stat[process_name] += process_duration
            else:
                self.process_stat[process_name] = process_duration

            main_windows = []
            for i in range(start, end+1):
                me = self.session_events[i]
                flag, window_name = is_main_window(me, app)

                if flag:
                    main_windows.append((i, window_name))

            if len(main_windows) <= 0:
                if process_name in last_prcess_window:
                    #self.window_slices.append((process_name, last_prcess_window[process_name], start, end))
                    psection['window_section'].append({'window_name': last_prcess_window[process_name], 'start': start, 'end': end})
                else:
                    psection['window_section'].append({'window_name': '', 'start': start, 'end': end})
                    #self.window_slices.append((process_name, process_name, start, end))
                    #print last_prcess_window[process_name], start, end
                continue

            slices = util.list_slice(main_windows, lambda k: k[1])
            for sidx, s in enumerate(slices):
                idx1 = main_windows[s[1]][0]
                idx2 = main_windows[s[2]][0]
                window_name = s[0]

                if sidx == 0:
                    idx1 = start

                if sidx == len(slices) - 1:
                    idx2 = end
                else:
                    idx2 = main_windows[slices[sidx+1][1]][0] - 1

                #self.window_slices.append((process_name, s[0], idx1, idx2))
                psection['window_section'].append({'window_name': s[0], 'start': idx1, 'end': idx2})
                #print s[0], idx1, idx2

            if len(self.window_slices) > 0:
                last_prcess_window[process_name] = psection['window_section'][-1]['window_name'] #self.window_slices[-1][1]

        for pidx, psection in enumerate(self.sections):
            for widx, wsection in enumerate(psection['window_section']):
                wsections = psection['window_section']
                if wsection['window_name'] == '':
                    widx2 = widx + 1
                    while widx2 < len(wsections):
                        if wsections[widx2] != '':
                            wsection['window_name'] = wsections[widx2]['window_name']
                            break
                        widx2 += 1

                    if widx2 >= len(wsections):
                        flag = False
                        for pidx2 in range(pidx+1, len(self.sections)):
                            if self.sections[pidx2]['process_name'] == psection['process_name']:
                                for wsection2 in self.sections[pidx2]['window_section']:
                                    if wsection2['window_name'] != '':
                                        wsection['window_name'] = wsection2['window_name']
                                        flag = True
                                        break
                                if flag:
                                    break


    def stat_spree(self, psection, spree, last=False):
        stype = self.get_spree_type(spree)
        start, end = spree
        duration = time_util.time_diff(self.session_events[start]['timestamp'], self.session_events[end]['timestamp'])

        self.spree_interval.append(duration)
        
        #self.activity_stat[process_name]['Understanding'] += self.session_events[end]['duration']
        process_name = psection['process_name']

        if stype.startswith('Navigation'):
            self.activity_stat[process_name]['Navigation'] += duration
            psection['Navigation'] += duration

            self.spree_categories['Navigation'] += 1

            if not last:
                self.activity_stat[process_name]['Understanding'] += self.session_events[end]['duration']
                psection['Understanding'] += self.session_events[end]['duration']
        elif stype.startswith('Understanding'):
            self.activity_stat[process_name]['Understanding'] += duration
            psection['Understanding'] += duration
            self.spree_categories['Understanding'] += 1
            if not last:
                self.activity_stat[process_name]['Understanding'] += self.session_events[end]['duration']
                psection['Understanding'] += self.session_events[end]['duration']
        else:
            self.activity_stat[process_name]['Editing'] += duration
            psection['Editing'] += duration
            self.spree_categories['Editing'] += 1
            if not last:
                self.activity_stat[process_name]['Editing'] += self.session_events[end]['duration']
                psection['Editing'] += self.session_events[end]['duration']

    def split_by_rt(self, rt=1):
        self.sprees = []
        self.activity_stat = {}
        self.window_stat = {}

        for psection in self.sections:
            process_name = psection['process_name']
            psection['Understanding'] = 0
            psection['Navigation'] = 0
            psection['Editing'] = 0
            if process_name not in self.activity_stat:
                self.activity_stat[process_name] = {'Understanding': 0, 'Navigation': 0, 'Editing': 0}

            for wsection in psection['window_section']:
                window_name = wsection['window_name']
                start = wsection['start']
                end = wsection['end']

                key_window = process_name + '/' + window_name
                if key_window not in self.window_stat:
                    self.window_stat[key_window] = 0
                self.window_stat[key_window] += time_util.time_diff(self.session_events[start]['timestamp'], self.session_events[end]['timestamp'])

                spree_start = start
                for i in range(start, end):
                    if self.session_events[i]['duration'] > rt:
                        self.sprees.append((spree_start, i))
                        self.stat_spree(psection, (spree_start, i))
                        
                        spree_start = i + 1

                self.sprees.append((spree_start, end))
                self.stat_spree(psection, (spree_start, end), True)

    def get_spree_type(self, spree):
        spree_start, spree_end = spree

        total = spree_end - spree_start + 1
        spree_stat = {
            'total': spree_end - spree_start + 1,
            'MOUSE MOVE': 0,
            'MOUSE WHEEL': 0,
            'MOUSE LEFT DOWN': 0,
            'MOUSE LEFT UP': 0,
            'MOUSE RIGHT DOWN': 0,
            'MOUSE RIGHT UP': 0,
            'Alphanumeric': 0,
            'Shortcut Key': 0,
            'Search Key': 0,
            'Navigation Key': 0,
            'Function Key': 0,
            'Browsing': 0,
            'Searching': 0,
            'Understanding': 0
        }
        for i in range(spree_start, spree_end+1):
            e = self.session_events[i]
            if e['type'] == 'mouse':
                if e['event_name'] in spree_stat:
                    spree_stat[e['event_name']] += 1
                else:
                    spree_stat[e['event_name']] = 1
            elif e['type'] == 'key':
                c = get_key_event_category(e)
                if c in spree_stat:
                    spree_stat[c] += 1
                else:
                    spree_stat[c] = 1

        if spree_stat['MOUSE MOVE'] == total:
            return 'Understanding - Mouse Drifting'
        elif spree_stat['MOUSE WHEEL'] + spree_stat['MOUSE MOVE'] == total:
            return 'Navigation - Mouse Wheel'
        elif spree_stat['Alphanumeric'] == total:
            return 'Editing'
        else:
            for i in range(spree_start, spree_end+1):
                e = self.session_events[i]
                if e['type'] == 'mouse' and 'action_name' in e:
                    category = get_event_category(e)
                    spree_stat[category] += 1
            
            if spree_stat['Searching'] > 0:
                return 'Navigation - Searching'
            elif spree_stat['Browsing'] > 0 and spree_stat['MOUSE LEFT DOWN'] > 0 and spree_stat['Browsing'] * 1.0 / spree_stat['MOUSE LEFT DOWN'] > 0.5:
                return 'Navigation - Browsing'
            elif spree_stat['Alphanumeric'] * 1.0 / spree_stat['total'] > 0.2:
                return 'Editing'
            else:
                return 'Understanding'      

        #return spree_stat

    def switch_stat(self):
        self.process_switch = {}
        
        switch_len = [2, 3, 4]
        for slen in switch_len:
            self.process_switch[slen] = {}

        for pidx, psection in enumerate(self.sections):
            cur_proc = psection['process_name']

            for slen in switch_len:
                if pidx > len(self.sections) - slen:
                    continue

                proc_array = [get_process_category(cur_proc)]
                for i in range(1, slen):
                    next_psection = self.sections[pidx + i]
                    proc_array.append(get_process_category(next_psection['process_name']))

                si = psection['start']
                ei = self.sections[pidx + slen - 1]['end']
                duration = time_util.time_diff(self.session_events[si]['timestamp'], self.session_events[ei]['timestamp'])

                keystr = '->'.join(proc_array)
                if keystr not in self.process_switch[slen]:
                    self.process_switch[slen][keystr] = {}
                    self.process_switch[slen][keystr]['count'] = 0
                    self.process_switch[slen][keystr]['psections'] = set()
                    #self.process_switch[slen][keystr]['Understanding'] = 0
                    #self.process_switch[slen][keystr]['Navigation'] = 0
                    #self.process_switch[slen][keystr]['Editing'] = 0
                    #self.process_switch[slen][keystr]['Total'] = 0

                self.process_switch[slen][keystr]['count'] += 1
                self.process_switch[slen][keystr]['psections'] |= set([pidx+i for i in range(0, slen)])
        
        for slen in switch_len:
            for keystr in self.process_switch[slen]:
                self.process_switch[slen][keystr]['Understanding'] = 0
                self.process_switch[slen][keystr]['psections'] = list(self.process_switch[slen][keystr]['psections'])
                for psec_idx in self.process_switch[slen][keystr]['psections']:
                    self.process_switch[slen][keystr]['Understanding'] += self.sections[psec_idx]['Understanding']


if __name__ == '__main__':
    batch = False
    if batch:
        # logs = ['../data/WON', '../data/Argus', '../data/HTA', '../data/UED', '../data/Honda']
        logs = ['D:/baolingfeng/research/Hengtian201612/WON/']

        for log in logs:
            for logfile in os.listdir(log):
                if logfile.endswith('.db3') is False:
                    continue

                try:
                    print os.path.join(log, logfile)
                    em = EventManager(os.path.join(log, logfile), 1)
                    em.run()
                except:
                    print 'error:', logfile
    else:
        # data_path = '../data/Argus/log_YufeiAn'
        data_path = 'E:/baolingfeng/research/Hengtian201612/WON/151080-log.db3'
        # data_path = '../data/user1/log'
        em = EventManager(data_path)
        em.run()
